/**
 * Copyright (c) 2014 UK Science and Technology Facilities Council
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package eu.paasage.upperware.plangenerator.type;

/**
 * An enum of permitted task type:
 * <ul>
 * <li>CREATE</li>
 * <li>DELETE</li>
 * <li>UPDATE</li>
 * </ul>
 * @author Shirley Crompton (shirley.crompton@stfc.ac.uk)
 * org     UK Science and Technology Facilities Council
 * project PaaSage
 *
 */
public enum TaskType {
	CREATE,
	DELETE,
	UPDATE, 
}
